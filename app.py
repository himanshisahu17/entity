from flask import Flask, jsonify, request,render_template,Response
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
import json
from bson import json_util

app = Flask(__name__)

connection = MongoClient('localhost', 27017)

db = connection['Rest_db']
col = db['entity']
#
# @app.route('/')
# def hello():
#     db = connection['Rest_db']
#     col = db['entity']
#
#     for i in col.find():
#         print(i)
#
#     return "hello"

@app.route('/entity/', methods=['GET'])
def get_all_frameworks():

    try :
        output=[]
        for i in col.find():
            output.append({'name': i['name'], 'description': i['description']})
        print(output)
        return jsonify({'result': output})
    except Exception as e:
        return jsonify({'Error':str(e)})
    #
    # try:
    # try:
    #     framework = db.col
    #
    #     output = []
    #
    #     for q in framework.find():
    #         output.append({'name': q['name'], 'description': q['description']})
    #
    #     return jsonify({'result': output})
    # except Exception as e:
    #     return jsonify({'Error': str(e)})


@app.route('/entityparam/<name>', methods=['GET'])
def get_one_framework(name):
    # name1=name
    # try:
    #     output = []
    #     for i in col.find_one():
    #         output.append({'name': i['name'], 'description': i['description']})
    #     print(output)
    #     return jsonify({'result': output})
    # except Exception as e:
    #     return jsonify({'Error': str(e)})

    name1 = name
    c = col.find_one({'name': str(name1)})
    print(c)
    if c:
        data_dict = { 'data': c}
    ff = json_util.dumps(data_dict)
    return Response(ff)

    # try:
    #     framework = db.col
    #
    #     q = framework.find_one({'name': str(name1)})
    #     print(q)
    #     if q:
    #         output = {'name': q['name'], 'description': q['description']}
    #     return jsonify({'result': output})
    # except Exception as e:
    #     return jsonify({'Error': str(e)})


@app.route('/entity', methods=['POST'])
def add_framework():
    try:
        framework = db.col

        name = request.json['name']
        description = request.json['description']

        framework_id = framework.insert({'name': name, 'description': description})
        new_framework = framework.find_one({'_id': framework_id})

        output = {'name': new_framework['name'], 'description': new_framework['description']}

        return jsonify({'result': output})
    except Exception as e:
        return jsonify({'Error': str(e)})


"""

@app.route('/entity/ObjectId(<name>)', methods=['PUT'])
def update_framework(ObjectId('name')):
    try:
        framework = db.col 

        q = framework.find({"_id":ObjectId(name)})

        if q:
            name = request.json['name']
            description = request.json['description']

            framework_id = framework.updateOne({'name' : name}, {'$set':{'name' : name, 'description' : description}})

            new_framework = framework.find_one({'_id' : framework_id})

            output = {'name' : new_framework['name'], 'description' : new_framework['description', data ]}

        return jsonify({'result' : output})
    except Exception as e:
        return jsonify({'Error': str(e)})

        """


@app.route('/entity/<name>', methods=['DELETE'])
def delete_one_framework(name):
    try:

        framework = db.col

        if framework.find_one({'name': name}) == None:
            raise Exception
        else:
            framework.delete_one({'name': name})
            output = "{} deleted successfully!".format(name)

        return jsonify({'result': output})
    except Exception as e:
        return jsonify({'Error': str(e)})


if __name__ == '__main__':
    app.run(debug=True)
